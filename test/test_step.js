var infopack = require('infopack');
var generator = require('../');

/**
 * Evalute function will run a single step, see more info: https://gitlab.com/sbplatform/infopack
 */
infopack.evaluate(generator.step({}))
    .then(function(result) {
        console.log(result.logs);
    })
    .catch((err) => {
        console.error(err);
    });
